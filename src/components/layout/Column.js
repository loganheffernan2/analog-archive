import React from 'react'
import { Col } from 'react-flexbox-grid'

const Column = props => <Col {...props} />

export default Column
