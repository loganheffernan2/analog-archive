---
title: PXL 2000
itemType: gallery
poster: /archive/media/screen-shot-2017-04-04-at-11.17.02-am.png
keywords:
  - value: New York
  - value: Family
  - value: 1980s
date: 10.8.18
related: []
images:
  - caption: test caption
    image: /archive/media/1519656a4fae802bc0a3f1ff0439f0ea.jpg
  - caption: test caption 2
    image: /archive/media/fp3305-pxl-camcorder.jpg
  - image: /archive/media/screen-shot-2017-04-04-at-11.17.02-am.png
---
Fisher-Price 3300, PXL 2000 Camcorder; 

Sold as Fisher-Price PXL2000/Fisher-Price PixelVision/Sanwa Sanpix1000/"KiddieCorder"/Coca-Cola Georgia. 

Toy, Sanyo LA 7306M CCD sensor with scannning 120 x 90 pixels in b/w 15 times a second with 180 kHz clock frequency, recorded (reduced to 160 kHz) on a standard Philips C90 tape cassette at a nine times faster speed than audio (4,75 cm/s) for up to ~11 minutes recording time, the image is "windowboxed", meaning it has a black border around all sides of the picture. Built-in RF modulator for US standard on channel 3 or 4, using AA type batteries. 

Compare with Model 3305 PXL-2000 Camcorder Deluxe System.
