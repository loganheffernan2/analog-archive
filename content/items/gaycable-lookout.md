---
title: 'GCN Lookout!'
itemType: video
poster: https://archive.org/download/GCN-Lookout-1990/GCN-Lookout-1990.thumbs/Gay%20Cable%20Network%20-%20Gay%20USA%20-%20Lookout%21%20%281990%29_000297.jpg
collections: []
keywords:
  - value: New York City
  - value: LGBTQ
  - value: 90s
  - value: AIDS
  - value: Activism
related: []
annotations:
  - body: >-
      (Clip #1)
    end: '0:02:10'
    start: '0:01:00'

mediaUrl: 'https://archive.org/download/GCN-Lookout-1990/Gay%20Cable%20Network%20-%20Gay%20USA%20-%20Lookout%21%20%281990%29.mp4'
---
GCN Clip
