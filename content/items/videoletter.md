---
title: 'International Videoletter: Santa Cruz'
itemType: video
poster: >-
  https://archive.org/download/international-videoletter-santa-cruz/international-videoletter-santa-cruz.thumbs/videoletter_000001.jpg
collections: []
keywords:
  - value: Outside Agitators
  - value: Politics
  - value: Roots of Video
  - value: Kartemquin Films
related:
  - items:
      - item: Videoletter Pamphlet
    title: Videoletter Pamphlet
annotations:
  - body: >-
      (Archive Party A) A woman is at a desk writing. Soft piano music plays in
      the background. Behind her on the wall are various posters. They read:
      “Solidarity with the Women of Indochina.” “Teamsters No!” “International
      Women’s Day.”
    end: '0:01:30'
    start: '0:00:00'
  - body: >-
      A woman holds up hand printed titles: “International Video Letters.”
      “Santa Cruz Women’s Media Collective.”
    end: '0:01:30'
    start: '0:01:11'
  - body: >-
      Another woman sits on the floor with a dog. She says her name is Ann. The
      audio is quite low. She notes a group of people from the Women’s Media
      Collective have been taping a new series of lectures on politics in the
      United States. This video letter is part of lecture we taped last night, a
      survey of all the political video groups in the Santa Cruz area.
    end: '0:02:12'
    start: '0:02:07'
  - body: >-
      Nec nisl malesuada ad phasellus a turpis mus et adipiscing id nec
      consectetur gravida ac montes eu a laoreet pulvinar consectetur iaculis mi
      inceptos.
    end: '0:02:00'
    images:
      - image: /archive/media/__ia_thumb.jpg
      - image: >-
          /archive/media/mv5bnzuymgnmoditnmjhys00yzdlltlmmgutngqymtkyytjhntmyxkeyxkfqcgdeqxvynzi4mdmymtu-._v1_.jpg
    start: '0:01:45'
  - body: >-
      (Archive Party B) Deanne from Santa Cruz Women Against Rape. Their agenda
      is to study how rape relates to capitalism, developing alternative ways to
      respond to rapists, and to educate the public on rape. They’ve also
      established a 24-hour support line for women who’ve been attacked.
    end: '0:23:56'
    start: '0:17:55'
mediaUrl: >-
  https://archive.org/download/international-videoletter-santa-cruz/videoletter.mp4
---
A revolutionary project, VIDEOLETTERS brought 27 women's video groups from ultimately 17 communities together to broadcast news, stories and struggles of feminism, 1975-77. (Ariel Dougherty)

Format: 1/2 in. reel-to-reel
