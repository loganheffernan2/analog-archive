---
title: Are You With Me
itemType: video
poster: /archive/media/__ia_thumb.jpg
annotations:
  - body: >-
      Hendrerit urna a habitasse euismod dolor nec sagittis nunc fusce varius
      urna ullamcorper dui molestie scelerisque.
    end: '0:00:50'
    start: '0:00:30'
  - body: >-
      Nec nisl malesuada ad phasellus a turpis mus et adipiscing id nec
      consectetur gravida ac montes eu a laoreet pulvinar consectetur iaculis mi
      inceptos.
    end: '0:02:00'
    start: '0:01:55'
mediaUrl: 'https://archive.org/download/AreYouWithMe_201805/Are_You_With_Me.ogv'
---
This 17-minute video, released during the AIDS crisis, aimed to increase awareness of [HIV/AIDS](https://cnn.com) transmission pathways and encourage safe sex practices. The film focuses on an African-American single mother, who works as a nurse, and her teenage daughter who is troubled by the death of a friend’s sister due to AIDS. After an uncomfortable conversation, both mother and daughter decide to ask their respective sexual partners to use condoms. Produced by AIDSFILMS with support from Wallach and other foundations, it was directed by M. Neema Barnette and features a jazz score by Lawrence “Butch” Morris.

— *Alberto*
