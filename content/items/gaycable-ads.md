---
title: 'Gay Cable News Ads'
itemType: video
poster: https://archive.org/download/gcn-ads/gcn-ads.thumbs/gcn-ads_000001.jpg
collections: []
keywords:
  - value: New York City
  - value: LGBTQ
  - value: 90s
  - value: AIDS
  - value: Activism
related: []
annotations:
  - body: >-
      (Clip #1)
    end: '0:02:10'
    start: '0:01:00'

mediaUrl: 'https://archive.org/download/gcn-ads/gcn-ads.mp4'
---
GCN Ads
